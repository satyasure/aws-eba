####################################################################

# Creating 3 EC2 Instances:

####################################################################
# Download the private key file to your local machine
resource "aws_instance" "instance" {
  count                = length(aws_subnet.public_subnet.*.id)
  ami                  = data.aws_ami.amazon_linux.id
  instance_type        = var.instance_type
  subnet_id            = element(aws_subnet.public_subnet.*.id, count.index)
  security_groups      = [aws_security_group.sg.id, ]
  key_name             = "tf_keypair"
  iam_instance_profile = aws_iam_instance_profile.nginx_instance_profile.name
    user_data = <<-EOF
                   sudo yum update -y
                   sudo amazon-linux-extras install nginx1
                   sudo systemctl start nginx
                   sudo systemctl enable nginx
                EOF
  tags = {
    "Name"        = "Instance-${count.index}"
    "Environment" = "Test"
    "CreatedBy"   = "Terraform"
  }

  timeouts {
    create = "10m"
  }

}

/*
resource "aws_instance" "ubuntu" {
  count                  = var.ubuntu_ec2_enabled
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "m5d.xlarge"
  key_name               = aws_key_pair.sshkey[0].key_name
  vpc_security_group_ids = ["${aws_security_group.allow_all.id}"]
  availability_zone      = data.aws_availability_zones.available.names[0]
  user_data              = file("${path.module}/userdata.sh")

  tags = {
    Terraformed = "true"
    Name        = "ubuntu"
  }

  lifecycle {
    ignore_changes        = [ebs_optimized, volume_tags, ebs_block_device]
    create_before_destroy = true
    prevent_destroy       = false
  }

  provisioner "file" {
    source      = "./userdata.sh"
    destination = "~/userdata.sh"

    connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = file("./tf_keypair.pem")
      host        = self.public_dns
    }
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update -qq",
      "sudo apt-get -qq install python-pip build-essential golang-go > /dev/null",
      "sudo apt-get -qq install net-tools unzip ansible expect awscli > /dev/null",
      "wget https://releases.hashicorp.com/terraform/0.11.11/terraform_0.11.11_linux_amd64.zip",
      "unzip terraform_0.11.11_linux_amd64.zip",
      "sudo mv terraform /usr/local/bin/",
      "pip install --quiet --upgrade pip",
      "sudo apt-get -qq install /tmp/GlobalProtect_deb-4.1.2.0-6.deb > /dev/null",
      "mkdir code && cd code",
      "git clone --quiet https://github.com/heldersepu/hs-scripts.git",
      "git clone --quiet https://github.com/jamesob/desk.git",
      "cd desk && sudo make install",
      "printf \"\n# Hook for desk activation\n\" >> ~/.bashrc",
      "echo '[ -n \"$DESK_ENV\" ] && source \"$DESK_ENV\" || true' >> ~/.bashrc",
      "echo '' > ~/.ssh/known_hosts",
      "sudo rm -f /etc/update-motd.d/*",
      "echo ''",
      "go version",
      "aws --version",
      "desk --version",
      "make --version",
      "ansible --version",
      "terraform --version",
      "echo ${self.public_dns}",
      "echo ''",
    ]

    connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = file("./tf_keypair.pem")
      host        = self.public_dns
    }
  }

  provisioner "file" {
    source      = "~/.aws"
    destination = "~"

    connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = file("./tf_keypair.pem")
      host        = self.public_dns
    }
  }

}

*/
####################################################################

# Creating 3 Elastic IPs:

####################################################################

resource "aws_eip" "eip" {
  count            = length(aws_instance.instance.*.id)
  instance         = element(aws_instance.instance.*.id, count.index)
  public_ipv4_pool = "amazon"
  vpc              = true

  tags = {
    "Name" = "EIP-${count.index}"
  }
}

####################################################################

# Creating EIP association with EC2 Instances:

####################################################################

resource "aws_eip_association" "eip_association" {
  count         = length(aws_eip.eip)
  instance_id   = element(aws_instance.instance.*.id, count.index)
  allocation_id = element(aws_eip.eip.*.id, count.index)
}