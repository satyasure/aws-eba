###################################################

# Fetching all availability zones in apsouth

###################################################

data "aws_availability_zones" "azs" {}


data "aws_ami" "amazon_linux" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["amazon"]
}

data "aws_key_pair" "tf_keypair" {
  key_name = "tf_keypair"
}