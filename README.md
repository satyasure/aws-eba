Set up a highly available auto-scaling group of 3 AWS EC2s running nginx to serve public web content behind a public ALB in the Singapore region using Terraform, 

We will follow below steps:

1. Create an S3 bucket for storing the web content, and upload the necessary files to the bucket.

2. Create an IAM role with permissions to access the S3 bucket, and attach the role to the EC2 instances.

3. Create a security group that allows incoming traffic on port 80 and 443, and outgoing traffic to the S3 bucket.

4. Create an ALB with a target group that points to the auto-scaling group instances.

5. Create a launch configuration that specifies the EC2 instance configuration, including the AMI, instance type, IAM role, and user data script that downloads the web content from the S3 bucket.

6. Create an auto-scaling group that uses the launch configuration, with a desired capacity of 3 and a maximum capacity of 10.

7. Associate the auto-scaling group instances with the target group of the ALB.


Steps to follow:

Set up a Terraform environment:
Create a directory for the project and create a main.tf file in it to define the AWS resources.

Configure the provider:
Define the AWS provider in the main.tf file to specify the region and access keys.

Create a VPC:
Define a VPC resource in the main.tf file. This should include a CIDR block, an internet gateway, and route tables.

Create subnets:
Define three subnets in the VPC, one for each availability zone.

Create an S3 bucket:
Define an S3 bucket resource in the main.tf file, and set it to private.

Create an ALB:
Define an Application Load Balancer resource in the main.tf file, and set it up to listen on port 80.

Create a Launch Configuration:
Define a Launch Configuration resource in the main.tf file to specify the AMI, instance type, and user data script to download and configure nginx.

Create an Auto Scaling Group:
Define an Auto Scaling Group resource in the main.tf file to specify the minimum and maximum number of instances, the desired capacity, and the Launch Configuration.

Define Security Groups:
Define security groups for the ALB and EC2 instances to allow incoming traffic on port 80 and restrict access to only the ALB.

Output Load Balancer DNS:
Add an output to the main.tf file to display the Load Balancer DNS name.