terraform {
      required_providers {
        aws = {
          source  = "hashicorp/aws"
        }
      }
    }
# Configure AWS provider:
provider "aws" {
      region  = "ap-southeast-1"
      profile = "default"
      }